﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.SharedBus.Models;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomerController : Controller
    {
        private readonly HubConnection _connection;
        private ILogger<CustomerController> _logger;

        public CustomerController(ILogger<CustomerController> logger)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/customer")
                .Build();

            _logger = logger;
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponseBusModel>> GetCustomerAsync(Guid id)
        {
            CustomerResponseBusModel? customer = null;
            try
            {
                await _connection.StartAsync();
                customer = await _connection.InvokeAsync<CustomerResponseBusModel>("GetCustomerById", id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(customer);
        }

        [HttpGet("customers")]
        public async Task<ActionResult<List<CustomerResponseBusModel>>> GetCustomersAsync()
        {
            List<CustomerShortResponseBusModel> customers = new();
            try
            {
                await _connection.StartAsync();
                customers = await _connection.InvokeAsync<List<CustomerShortResponseBusModel>>("GetCustomers");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(customers);
        }

        [HttpPost]
        public async Task<ActionResult<CustomerResponseBusModel>> CreateCustomerAsync(CreateOrEditCustomerRequestBusModel request)
        {
            Guid? customerId = null;
            try
            {
                await _connection.StartAsync();
                customerId = await _connection.InvokeAsync<Guid>("CreateCustomerAsync", request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            if (!customerId.HasValue)
            {
                return BadRequest();
            }

            return Ok(customerId);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequestBusModel request)
        {
            var result = false;
            try
            {
                await _connection.StartAsync();
                result = await _connection.InvokeAsync<bool>("EditCustomersAsync", id, request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var result = false;
            try
            {
                await _connection.StartAsync();
                result = await _connection.InvokeAsync<bool>("DeleteCustomerAsync", id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
