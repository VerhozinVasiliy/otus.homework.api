﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Abstractions
{
    public interface ICustomerService
    {
        Task<List<CustomerShortResponse>> GetCustomersAsync();

        Task<CustomerResponse> GetCustomerAsync(Guid id);

        Task<Customer> CreateCustomerAsync(CreateOrEditCustomerRequest request);

        Task<bool> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request);

        Task<bool> DeleteCustomerAsync(Guid id);
    }
}
