﻿using Grpc.Core;
using GrpcService;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Grpc
{
    public class CustomerGrpcService : ProtoCustomer.ProtoCustomerBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly ICustomerService _customerService;

        public CustomerGrpcService(ILogger<CustomerService> logger, ICustomerService customerService)
        {
            _logger = logger;
            _customerService = customerService;
        }

        public override async Task<CustomerReply> GetCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Guid, out Guid parsedGuid))
            {
                _logger.LogError($"Cant parsed Guid {request.Guid}");
                return null;
            }
            var response = await _customerService.GetCustomerAsync(parsedGuid);
            
            var cr = new CustomerReply
            {
                Id = response.Id.ToString(),
                Email = response.Email,
                Firstname = response.FirstName,
                Lastname = response.LastName,
            };

            if (response.Preferences != null && response.Preferences.Any())
            {
                cr.Preference.AddRange(response.Preferences.Select(s => new PreferenceReply { Id = s.Id.ToString(), Name = s.Name }));
            }

            if (response.PromoCodes != null && response.PromoCodes.Any())
            {
                cr.Promocode.AddRange(response.PromoCodes.Select(s => 
                    new PomocodeReply { 
                        Id = s.Id.ToString(), 
                        Code = s.Code,
                        Serviceinfo = s.ServiceInfo,
                        Begindate = s.BeginDate,
                        Enddate = s.EndDate,
                        Partnername = s.PartnerName
                    }));
            }

            return cr;
        }

        public override async Task<CustomersReply> GetCustomers(EmpltyRequest request, ServerCallContext context)
        {
            var response = await _customerService.GetCustomersAsync();
            var reply = new CustomersReply();
            foreach (var item in response)
            {
                reply.Customer.Add(new CustomerReply
                {
                    Id = item.Id.ToString(),
                    Email = item.Email,
                    Firstname = item.FirstName,
                    Lastname = item.LastName,
                });
            }
            return reply;
        }

        public override async Task<CreateCustomerReply> CreateCustomer(CreateEditCustomerRequest request, ServerCallContext context)
        {
            var createModel = new Models.CreateOrEditCustomerRequest
            {
                FirstName = request.Firstname,
                Email = request.Email,
                LastName = request.Lastname,
                PreferenceIds = new List<Guid>()
            };
            if (request.Preferenceids != null && request.Preferenceids.Id.Any())
            {
                foreach (var guid in request.Preferenceids.Id)
                {
                    if (!Guid.TryParse(guid, out Guid parsedGuid))
                    {
                        _logger.LogError($"Cant parse Guid {guid}");
                        continue;
                    }
                    createModel.PreferenceIds.Add(parsedGuid);
                }
            }

            var response = await _customerService.CreateCustomerAsync(createModel);
            var reply = new CreateCustomerReply { Id = response.Id.ToString() };
            return reply;
        }

        public override async Task<BoolReply> EditCustomer(CreateEditCustomerRequest request, ServerCallContext context)
        {
            var createModel = new Models.CreateOrEditCustomerRequest
            {
                FirstName = request.Firstname,
                Email = request.Email,
                LastName = request.Lastname,
                PreferenceIds = new List<Guid>()
            };
            if (request.Preferenceids != null && request.Preferenceids.Id.Any())
            {
                foreach (var guid in request.Preferenceids.Id)
                {
                    if (!Guid.TryParse(guid, out Guid parsedGuid))
                    {
                        _logger.LogError($"Cant parse Guid {guid}");
                        continue;
                    }
                    createModel.PreferenceIds.Add(parsedGuid);
                }
            }

            var response = await _customerService.EditCustomersAsync(Guid.Parse(request.Id), createModel);
            if (!response)
            {
                return new BoolReply { Result = false };
            }
            var reply = new BoolReply { Result = response };
            return reply;
        }

        public override async Task<BoolReply> DeleteCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Guid, out Guid parsedGuid))
            {
                _logger.LogError($"Cant parsed Guid {request.Guid}");
                return new BoolReply { Result = false };
            }
            var response = await _customerService.DeleteCustomerAsync(parsedGuid);
            var reply = new BoolReply { Result = response };
            return reply;
        }
    }
}
