﻿using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.SharedBus.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.SignalR
{
    public class CustomerHub : Hub
    {
        private readonly ICustomerService _customerService;

        public CustomerHub(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<CustomerResponseBusModel> GetCustomerById(Guid id)
        {
            var response = await _customerService.GetCustomerAsync(id);
            return CustomerMapper.MapToBus(response);
        }

        public async Task<List<CustomerShortResponseBusModel>> GetCustomers()
        {
            var customers = await _customerService.GetCustomersAsync();
            var response = customers.Select(x => CustomerMapper.MapToBusShort(x)).ToList();
            return response;
        }

        public async Task<Guid> CreateCustomerAsync(CreateOrEditCustomerRequestBusModel request)
        {
            var customer = await _customerService.CreateCustomerAsync(CustomerMapper.MapFromBusCreateEditRequest(request));
            return customer.Id;
        }

        public async Task<bool> EditCustomersAsync(Guid id, CreateOrEditCustomerRequestBusModel request)
        {
            var result = await _customerService.EditCustomersAsync(id, CustomerMapper.MapFromBusCreateEditRequest(request));
            if (!result)
                return false;

            return true;
        }

        public async Task<bool> DeleteCustomerAsync(Guid id)
        {
            var result = await _customerService.DeleteCustomerAsync(id);
            if (!result)
                return false;

            return true;
        }
    }
}
