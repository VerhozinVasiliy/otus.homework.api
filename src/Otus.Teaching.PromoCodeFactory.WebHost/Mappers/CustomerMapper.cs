﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.SharedBus.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class CustomerMapper
    {

        public static Customer MapFromModel(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            return customer;
        }

        public static CustomerResponseBusModel MapToBus(CustomerResponse customer)
        {
            return new CustomerResponseBusModel
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences.Select(x => new PreferenceResponseBusModel()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList()
            };
        }

        public static CustomerShortResponseBusModel MapToBusShort(CustomerShortResponse customer)
        {
            return new CustomerShortResponseBusModel
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }

        public static CreateOrEditCustomerRequest MapFromBusCreateEditRequest(CreateOrEditCustomerRequestBusModel customer)
        {
            return new CreateOrEditCustomerRequest
            {
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PreferenceIds = customer.PreferenceIds
            };
        }
    }
}
