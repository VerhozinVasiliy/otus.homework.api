﻿namespace Otus.Teaching.PromoCodeFactory.SharedBus.Models
{
    public class CustomerResponseBusModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponseBusModel> Preferences { get; set; }
        public List<PromoCodeShortResponseBusModel> PromoCodes { get; set; }
    }
}
