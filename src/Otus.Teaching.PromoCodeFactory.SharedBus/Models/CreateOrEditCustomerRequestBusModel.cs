﻿namespace Otus.Teaching.PromoCodeFactory.SharedBus.Models
{
    public class CreateOrEditCustomerRequestBusModel
    {
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;

        public List<Guid> PreferenceIds { get; set; }
    }
}
