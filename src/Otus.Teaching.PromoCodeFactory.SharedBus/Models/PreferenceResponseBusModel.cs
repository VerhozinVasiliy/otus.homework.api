﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SharedBus.Models
{
    public class PreferenceResponseBusModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
