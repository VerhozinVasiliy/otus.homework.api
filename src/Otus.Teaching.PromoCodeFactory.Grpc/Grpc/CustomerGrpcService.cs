﻿using Grpc.Core;
using Grpc.Net.Client;
using GrpcService;
using Otus.Teaching.PromoCodeFactory.Grpc.Grpc.Abstractions;
using Otus.Teaching.PromoCodeFactory.Grpc.Settings;
using Otus.Teaching.PromoCodeFactory.SharedBus.Models;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Grpc
{
    public class CustomerGrpcService : ICustomerGrpc
    {
        private readonly ILogger<CustomerGrpcService> _logger;
        private readonly ApplicationSettings _settings;

        public CustomerGrpcService(ApplicationSettings settings, ILogger<CustomerGrpcService> logger)
        {
            _settings = settings;
            _logger = logger;
        }

        public async Task<CustomerResponseBusModel> GetCustomer(Guid id)
        {
            using var channel = GrpcChannel.ForAddress(_settings.GrpcServerAdress);
            var client = new ProtoCustomer.ProtoCustomerClient(channel);

            var request = new GetCustomerRequest { Guid = id.ToString() };

            CustomerReply? reply = null;

            try
            {
                reply = await client.GetCustomerAsync(request);
            }
            catch (RpcException ex)
            {
                _logger.LogError($"Status code: {ex.Status.StatusCode}");
                _logger.LogError($"Message: {ex.Status.Detail}");
                return new CustomerResponseBusModel();
            }
            if (reply == null)
            {
                _logger.LogError($"Not found");
                return new CustomerResponseBusModel();
            }

            var model =  new CustomerResponseBusModel
            {
                Id = id,
                FirstName = reply.Firstname,
                LastName = reply.Lastname,
                Email = reply.Email
            };
            if (reply.Preference.Any())
            {
                model.Preferences = new List<PreferenceResponseBusModel>();
                model.Preferences.AddRange(reply.Preference.Select(x => new PreferenceResponseBusModel { Id = Guid.Parse(x.Id), Name = x.Name }));
            }
            if (reply.Promocode.Any())
            {
                model.PromoCodes = new List<PromoCodeShortResponseBusModel>();
                model.PromoCodes.AddRange(reply.Promocode.Select(s => 
                    new PromoCodeShortResponseBusModel
                    { 
                        Id = Guid.Parse(s.Id),
                        Code = s.Code,
                        ServiceInfo = s.Serviceinfo,
                        BeginDate = s.Begindate,
                        EndDate = s.Enddate,
                        PartnerName = s.Partnername
                    }));
            }

            return model;
        }

        public async Task<List<CustomerShortResponseBusModel>> GetCustomers()
        {
            using var channel = GrpcChannel.ForAddress(_settings.GrpcServerAdress);
            var client = new ProtoCustomer.ProtoCustomerClient(channel);

            var request = new EmpltyRequest();

            CustomersReply? reply = null;

            try
            {
                reply = await client.GetCustomersAsync(request);
            }
            catch (RpcException ex)
            {
                _logger.LogError($"Status code: {ex.Status.StatusCode}");
                _logger.LogError($"Message: {ex.Status.Detail}");
                return new List<CustomerShortResponseBusModel>();
            }

            var list = new List<CustomerShortResponseBusModel>();
            foreach (var item in reply.Customer)
            {
                if (!Guid.TryParse(item.Id, out Guid parsedGuid))
                {
                    _logger.LogError($"Cant parsed guid {item.Id}");
                    continue;
                }
                list.Add(new CustomerShortResponseBusModel
                {
                    Id = Guid.Parse(item.Id),
                    FirstName = item.Firstname,
                    LastName = item.Lastname,
                    Email = item.Email
                });
            }

            return list;
        }

        public async Task<Guid?> CreateCustomer(CreateOrEditCustomerRequestBusModel createCustomerModel)
        {
            using var channel = GrpcChannel.ForAddress(_settings.GrpcServerAdress);
            var client = new ProtoCustomer.ProtoCustomerClient(channel);

            var request = new CreateEditCustomerRequest {
                Id = string.Empty,
                Firstname = createCustomerModel.FirstName,
                Email = createCustomerModel.Email,
                Lastname = createCustomerModel.LastName,
                Preferenceids = new PreferenceIdRequest()
            };
            if (createCustomerModel.PreferenceIds != null && createCustomerModel.PreferenceIds.Any())
            {
                request.Preferenceids.Id.AddRange(createCustomerModel.PreferenceIds.Select(s => s.ToString()));
            }

            CreateCustomerReply? reply = null;
            try
            {
                reply = await client.CreateCustomerAsync(request);
            }
            catch (RpcException ex)
            {
                _logger.LogError($"Status code: {ex.Status.StatusCode}");
                _logger.LogError($"Message: {ex.Status.Detail}");
                return null;
            }

            bool resultParse = Guid.TryParse(reply.Id, out Guid parsedGuid);
            if (reply == null && !resultParse)
            {
                return null;
            }

            return parsedGuid;
        }

        public async Task<bool> EditCustomers(Guid id, CreateOrEditCustomerRequestBusModel createCustomerModel)
        {
            using var channel = GrpcChannel.ForAddress(_settings.GrpcServerAdress);
            var client = new ProtoCustomer.ProtoCustomerClient(channel);

            var request = new CreateEditCustomerRequest
            {
                Id = id.ToString(),
                Firstname = createCustomerModel.FirstName,
                Email = createCustomerModel.Email,
                Lastname = createCustomerModel.LastName,
                Preferenceids = new PreferenceIdRequest()
            };
            if (createCustomerModel.PreferenceIds != null && createCustomerModel.PreferenceIds.Any())
            {
                request.Preferenceids.Id.AddRange(createCustomerModel.PreferenceIds.Select(s => s.ToString()));
            }

            BoolReply? reply = null;
            try
            {
                reply = await client.EditCustomerAsync(request);
            }
            catch (RpcException ex)
            {
                _logger.LogError($"Status code: {ex.Status.StatusCode}");
                _logger.LogError($"Message: {ex.Status.Detail}");
                return false;
            }

            return reply != null && reply.Result == true;
        }

        public async Task<bool> DeleteCustomer(Guid id)
        {
            using var channel = GrpcChannel.ForAddress(_settings.GrpcServerAdress);
            var client = new ProtoCustomer.ProtoCustomerClient(channel);

            var request = new GetCustomerRequest { Guid = id.ToString() };

            BoolReply? reply = null;

            try
            {
                reply = await client.DeleteCustomerAsync(request);
            }
            catch (RpcException ex)
            {
                _logger.LogError($"Status code: {ex.Status.StatusCode}");
                _logger.LogError($"Message: {ex.Status.Detail}");
                return false;
            }

            return reply != null && reply.Result == true;
        }
    }
}
