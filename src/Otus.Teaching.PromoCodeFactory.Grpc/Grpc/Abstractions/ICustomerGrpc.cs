﻿using Otus.Teaching.PromoCodeFactory.SharedBus.Models;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Grpc.Abstractions
{
    public interface ICustomerGrpc
    {
        Task<CustomerResponseBusModel> GetCustomer(Guid id);

        Task<List<CustomerShortResponseBusModel>> GetCustomers();
        Task<Guid?> CreateCustomer(CreateOrEditCustomerRequestBusModel createCustomerModel);

        Task<bool> EditCustomers(Guid id, CreateOrEditCustomerRequestBusModel createCustomerModel);

        Task<bool> DeleteCustomer(Guid id);
    }
}
