﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Grpc.Grpc.Abstractions;
using Otus.Teaching.PromoCodeFactory.SharedBus.Models;

namespace Otus.Teaching.PromoCodeFactory.Grpc.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomerController : Controller
    {
        private readonly ILogger<CustomerController> _logger;
        private readonly ICustomerGrpc _customerService;

        public CustomerController(ILogger<CustomerController> logger, ICustomerGrpc customerService)
        {
            _logger = logger;
            _customerService = customerService;
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponseBusModel>> GetCustomerAsync(Guid id)
        {
            CustomerResponseBusModel? customer = await _customerService.GetCustomer(id);
            return Ok(customer);
        }

        [HttpGet("customers")]
        public async Task<ActionResult<List<CustomerResponseBusModel>>> GetCustomersAsync()
        {
            var customers = await _customerService.GetCustomers();
            return Ok(customers);
        }

        [HttpPost]
        public async Task<ActionResult> CreateCustomerAsync(CreateOrEditCustomerRequestBusModel request)
        {
            var customerId = await _customerService.CreateCustomer(request);
            if (customerId == null)
            {
                return BadRequest();
            }
            return Ok(customerId);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequestBusModel request)
        {
            var result = await _customerService.EditCustomers(id, request);
            if (!result)
                return NotFound();

            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var result = await _customerService.DeleteCustomer(id);
            if (!result)
                return NotFound();

            return NoContent();
        }
    }
}
