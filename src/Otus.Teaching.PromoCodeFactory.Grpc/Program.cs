using Otus.Teaching.PromoCodeFactory.Grpc.Grpc;
using Otus.Teaching.PromoCodeFactory.Grpc.Grpc.Abstractions;
using Otus.Teaching.PromoCodeFactory.Grpc.Settings;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var configuration = builder.Configuration;
var applicationSettings = configuration.Get<ApplicationSettings>();
builder.Services.AddSingleton(applicationSettings);

builder.Services.AddTransient<ICustomerGrpc, CustomerGrpcService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
