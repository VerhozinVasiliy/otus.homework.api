﻿namespace Otus.Teaching.PromoCodeFactory.Grpc.Settings
{
    public class ApplicationSettings
    {
        public string GrpcServerAdress { get; set; } = string.Empty;
    }
}
